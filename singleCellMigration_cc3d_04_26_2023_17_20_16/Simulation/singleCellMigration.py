
from cc3d import CompuCellSetup
        

from singleCellMigrationSteppables import singleCellMigrationSteppable

CompuCellSetup.register_steppable(steppable=singleCellMigrationSteppable(frequency=1))



#from singleCellMigrationSteppables import VisualizationSteppable

#CompuCellSetup.register_steppable(steppable=VisualizationSteppable(frequency=1))

from singleCellMigrationSteppables import TrackCellCOMSteppable
CompuCellSetup.register_steppable(steppable=TrackCellCOMSteppable(frequency=1))

        
from singleCellMigrationSteppables import cellmitosisSteppable
CompuCellSetup.register_steppable(steppable=cellmitosisSteppable(frequency=1))

CompuCellSetup.run()
