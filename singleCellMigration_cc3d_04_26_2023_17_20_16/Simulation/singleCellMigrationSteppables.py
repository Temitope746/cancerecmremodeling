
from cc3d.core.PySteppables import *
import numpy as np
from numpy.random import random, normal
from random import uniform, seed

class singleCellMigrationSteppable(SteppableBasePy):

    def __init__(self,frequency=1):

        SteppableBasePy.__init__(self,frequency)

    def start(self):
        """
        any code in the start function runs before MCS=0
        """
        # # Add a single cell to the center of the simulation space
        first_cell = self.new_cell(self.CANCER)
        self.cell_field[120:140, 120:140, 0] = first_cell
        
        # Add initial target and lambda volume volume
        first_cell.targetVolume = 25
        first_cell.lambdaVolume = 2
        
        
        # setting new direction of cell
        for cell in self.cell_list:
            # force component pointing along X axis
            cell.lambdaVecX = 5000.1 * uniform(-0.5, 0.5)
            # force component pointing along Y axis
            cell.lambdaVecY = 5000.1 * uniform(-0.5, 0.5)
            

    def step(self,mcs):
        """
        type here the code that will run every frequency MCS
        :param mcs: current Monte Carlo step
        """
        
        for cell in self.cell_list:
            cell.targetVolume += 1
            
        # setting new direction of cell
        for cell in self.cell_list:
            # force component pointing along X axis
            cell.lambdaVecX = 5000.1 * uniform(-0.5, 0.5)
            # force component pointing along Y axis
            cell.lambdaVecY = 5000.1 * uniform(-0.5, 0.5)
            

    def finish(self):
        """
        Finish Function is called after the last MCS
        """
        
  
 

    def on_stop(self):
        # this gets called each time user stops simulation
        return


        
class cellmitosisSteppable(MitosisSteppableBase):
    def __init__(self,frequency=1):
        MitosisSteppableBase.__init__(self,frequency)

    def step(self, mcs):

        cells_to_divide = []
        
        if len(self.cell_list) < 100:
            
            for cell in self.cell_list:
                if cell.volume > 50:
                    cells_to_divide.append(cell)

            for cell in cells_to_divide:
                self.divide_cell_random_orientation(cell)
                
            # Other valid options
            # self.divide_cell_orientation_vector_based(cell,1,1,0)
            # self.divide_cell_along_major_axis(cell)
            # self.divide_cell_along_minor_axis(cell)
            
            
            #### Creating blob-clusters and rectangular 
            #####
            for i in range(10):
                # create a rectangular cell cluster
                x = i * 5
                y = 5
                for j in range(2):
                    #cell= self.new_cell(self.CANCER)
                    cell= self.new_cell()
                    self.cell_field[x+j, y, 0] = cell
                    
                    
            for i in range(10):
                # create a blob-shaped cell cluster
                x = i * 5
                y = 25
                for j in range(2):
                    #cell = self.new_cell(self.CANCER)
                    cell = self.new_cell()
                    self.cell_field[x+j, y, 0] = cell  
                   
            
            #### Comment above code for creating blob-clusters and rectangular
            
            # cluster1 = self.create_cluster(10, (10, 25, 25))
            # cluster2 = self.create_cluster(10, (40, 25, 25))
            
            
            # self.elongate_cluster(cluster1, 10)
            # self.elongate_cluster(cluster2, 10)
    
    # def create_cluster(self, num_cells, center):
        
        # cells = []
        # for i in range(num_cells):
            # cell = self.new_cell()
            # cells.append(cell)
            # self.cell_field[center[0]+i, center[1], center[2]] = cell
            
        # return cells
        

    # def elongate_cluster(self, cluster, num_steps):

        # for i in range(num_steps):
            # for cell in cluster:
                # pos = self.fetch_cell_by_id(cell)  # get cell position
                # self.cell_field[pos[0]+1, pos[1], pos[2]] = cell
                # self.cell_field[pos[0]-1, pos[1], pos[2]] = None
                



    def update_attributes(self):
        # reducing parent target volume
        self.parent_cell.targetVolume /= 2.0                  

        self.clone_parent_2_child()            

        # for more control of what gets copied from parent to child use cloneAttributes function
        # self.clone_attributes(source_cell=self.parent_cell, target_cell=self.child_cell, no_clone_key_dict_list=[attrib1, attrib2]) 
        
        if self.parent_cell.type==1:
            self.child_cell.type=1
        else:
            self.child_cell.type=1



class VisualizationSteppable(SteppableBasePy):
    def __init__(self, frequency=1):
        SteppableBasePy.__init__(self, frequency)

    def start(self):
        
        
        for cell in self.cell_list:
            
            # # Create plot window for volume and surface number of cells
            self.plot_win = self.add_new_plot_window(title='Average Volume And Surface',
                                                     x_axis_title='MonteCarlo Step (MCS)',
                                                     y_axis_title='Variables', 
                                                     x_scale_type='linear', 
                                                     y_scale_type='linear',
                                                     grid=True)
            
            self.plot_win.add_plot("MVol", style='Lines', color='red', size=5)
            self.plot_win.add_plot("MSur", style='Dots', color='green', size=5)
            
            
        


    def step(self, mcs):
        
        for cell in self.cell_list:
            # Update plots
            self.plot_win.add_data_point('MVol', mcs, len(self.cell_list_by_type(1)))
            self.plot_win.add_data_point('MSur', mcs, len(self.cell_list_by_type(2)))
        


    def finish(self):
        # save plots
        pass
        
        
        
        
        
class TrackCellCOMSteppable(SteppableBasePy):
    def __init__(self,frequency=1):
        SteppableBasePy.__init__(self,frequency)

    def start(self):     
        for cell in self.cell_list:
            cell.targetVolume =25
            cell.lambdaVolume = 2
            cell.targetSurface=20
            cell.lambdaSurface= 1
        
        # Approach 1: create a user controlled pixel-level field to hold the cell's old positions 
        self.create_scalar_field_py("cell_track")
        # Approach 2: make a plot of the cells positions
        self.plot_win = self.add_new_plot_window(title='Cancer Cell Migration Trajectory',
                                                 x_axis_title='X', x_scale_type='linear',
                                                 y_axis_title='Y', y_scale_type='linear',
                                                 grid=False)
        self.plot_win.add_plot("Track", style='dot', color='white', size=1)
        # make some dots to force the plot to autoscale like we want (0,0),(100,100)
        # arguments are (name of the data series, x, y)
        self.plot_win.add_data_point("Track",0,    0)
        self.plot_win.add_data_point("Track",0,  100)
        self.plot_win.add_data_point("Track",100,  0)
        self.plot_win.add_data_point("Track",100,100)
        
    def step(self,mcs):
        # just tracking the center of "acell" type every 100th MCS
        if mcs % 10 == 0:
            for cell in self.cell_list_by_type(self.CANCER):
                # this cell's center of mass (COM) is at (cell.xCOM,cell.yCOM,cell.zCOM)
                # Approach 1: using the field:
                field = self.field.cell_track
                field[int(cell.xCOM),int(cell.yCOM),int(cell.zCOM)] = mcs # just set the pixel to equal the number of mcs so far
                # Approach 2: using the plot:
                self.plot_win.add_data_point("Track",cell.xCOM,cell.yCOM)  # don't need int's