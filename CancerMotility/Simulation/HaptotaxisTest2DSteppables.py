# from PySteppables import *
# import CompuCell
# import sys
# import random 
# import math

from cc3d.core.PySteppables import *
from random import uniform, randrange, seed
from numpy.random import normal
from numpy import mean
import numpy as np
from pathlib import Path
import csv

seed(12345)

# from PlayerPython import *
# import CompuCellSetup
# from math import *



class HaptotaxisTest2DSteppable(SteppableBasePy):
    # add external potential for simulating random walk
    def __init__(self,frequency=10):
        SteppableBasePy.__init__(self,frequency)

    def start(self):
        print "This function is called once before simulation"
        
        # iterating over all cells in simulation        
        for cell in self.cellList:
            break 
            # Make sure ExternalPotential plugin is loaded
            # negative lambdaVecX makes force point in the positive direction
            cell.lambdaVecX=100.1*uniform(-0.5,0.5) # force component pointing along X axis 
            cell.lambdaVecY=100.1*uniform(-0.5,0.5) # force component pointing along Y axis 
#         cell.lambdaVecZ=0.0 # force component pointing along Z axis 
        
        
    def step(self,mcs):
        
        for cell in self.cellList:
            
            cell.lambdaVecX=100.1*uniform(-0.5,0.5) # force component pointing along X axis 
            cell.lambdaVecY=100.1*uniform(-0.5,0.5) # force component pointing along Y axis 
            # print 'cell.lambdaVecX=',cell.lambdaVecX,' cell.lambdaVecY=',cell.lambdaVecY
        
                     
class FiberConcentrationCaseARandom50(SteppableBasePy):
    def __init__(self,frequency=1):
        SteppableBasePy.__init__(self,frequency)
        
    def start(self):
        field_fiber      = self.getConcentrationField("fiber")
        #field_fiber_cl = self.getConcentrationField('fiber_cl')
        
        for x,y,z in self.everyPixel():   
            field_fiber[x,y,z]  = random.uniform(0, 1)
            #field_fiber_cl[x,y,z]  = 0
                
    def step(self,mcs):
        fiber_concentration     = 0
        #fiber_cl_concentration = 0  
        
        field_fiber      = self.getConcentrationField("fiber")
        #field_fiber_cl = self.getConcentrationField('fiber_cl')
        
        for x,y,z in self.everyPixel():
            fiber_concentration     += field_fiber[x,y,z]
            #fiber_cl_concentration += field_fiber_cl[x,y,z]
                    
        fileName='CaseA_Random_50.csv'
        try:
            fileHandle,fullFileName=self.openFileInSimulationOutputDirectory(fileName,"a")
        except IOError:
            print "Could not open file ", fileName," for writing. "
            return
            
        print >>fileHandle,mcs,",", fiber_concentration#,",", fiber_cl_concentration
        fileHandle.close()
            
    def finish(self):
        # this function may be called at the end of simulation - used very infrequently though
        return
    
class FiberConcentrationCaseAUniform50(SteppableBasePy):
    def __init__(self,frequency=1):
        SteppableBasePy.__init__(self,frequency)
        
    def start(self):
        field_fiber      = self.getConcentrationField("fiber")
        #field_fiber_cl = self.getConcentrationField('fiber_cl')
        
        #****Average 0.5 everywhere:
        for x,y,z in self.everyPixel():  
            field_fiber[x,y,z]  = 1.0
        #    field_fiber_cl[x,y,z]  = 0
        
        #****Half  0 Half 1
        for x,y,z in self.everyPixel():  
            if x >=150: 
                field_fiber[x,y,z]  = 0
                #field_fiber_cl[x,y,z]  = 0
            else:
                field_fiber[x,y,z]  = 1
                #field_fiber_cl[x,y,z]  = 0
                
    def step(self,mcs):
        fiber_concentration     = random.uniform(0, 1)
        #fiber_cl_concentration = 0  
        
        field_fiber      = self.getConcentrationField("fiber")
        #field_fiber_cl = self.getConcentrationField('fiber_cl')
        
        for x,y,z in self.everyPixel():
            fiber_concentration     += field_fiber[x,y,z]
            #fiber_cl_concentration += field_fiber_cl[x,y,z]
                
      
                    
        fileName='CaseA_Uniform_50.csv'
        try:
            fileHandle,fullFileName=self.openFileInSimulationOutputDirectory(fileName,"a")
        except IOError:
            print "Could not open file ", fileName," for writing. "
            return
            
        print >>fileHandle,mcs,",", fiber_concentration #,",", fiber_cl_concentration
        fileHandle.close()
            
    def finish(self):
        # this function may be called at the end of simulation - used very infrequently though
        return
    
class FiberConcentrationCaseBRandom25(SteppableBasePy):
    def __init__(self,frequency=1):
        SteppableBasePy.__init__(self,frequency)
        
    def start(self):
        field_fiber      = self.getConcentrationField("fiber")
        #field_fiber_cl = self.getConcentrationField('fiber_cl')
        
        for x,y,z in self.everyPixel():   
            field_fiber[x,y,z]  = random.uniform(0, 0.5)
            #field_fiber_cl[x,y,z]  = 0
                
    def step(self,mcs):
        fiber_concentration     = 0
        #fiber_cl_concentration = 0  
        
        field_fiber      = self.getConcentrationField("fiber")
        #field_fiber_cl = self.getConcentrationField('fiber_cl')
        
        for x,y,z in self.everyPixel():
            fiber_concentration     += field_fiber[x,y,z]
            #fiber_cl_concentration += field_fiber_cl[x,y,z]
                    
        fileName='CaseB_Random_25.csv'
        try:
            fileHandle,fullFileName=self.openFileInSimulationOutputDirectory(fileName,"a")
        except IOError:
            print "Could not open file ", fileName," for writing. "
            return
            
        print >>fileHandle,mcs,",", fiber_concentration#,",", fiber_cl_concentration
        fileHandle.close()
            
    def finish(self):
        # this function may be called at the end of simulation - used very infrequently though
        return
    
class FiberConcentrationCaseBRandom50(SteppableBasePy):
    def __init__(self,frequency=1):
        SteppableBasePy.__init__(self,frequency)
        
    def start(self):
        field_fiber      = self.getConcentrationField("fiber")
        #field_fiber_cl = self.getConcentrationField('fiber_cl')
        
        for x,y,z in self.everyPixel():   
            field_fiber[x,y,z]  = random.uniform(0.25, 0.75)
            #field_fiber_cl[x,y,z]  = 0
                
    def step(self,mcs):
        fiber_concentration     = 0
        #fiber_cl_concentration = 0  
        
        field_fiber      = self.getConcentrationField("fiber")
        #field_fiber_cl = self.getConcentrationField('fiber_cl')
        
        for x,y,z in self.everyPixel():
            fiber_concentration     += field_fiber[x,y,z]
            #fiber_cl_concentration += field_fiber_cl[x,y,z]
                    
        fileName='CaseB_Random_50.csv'
        try:
            fileHandle,fullFileName=self.openFileInSimulationOutputDirectory(fileName,"a")
        except IOError:
            print "Could not open file ", fileName," for writing. "
            return
            
        print >>fileHandle,mcs,",", fiber_concentration #,",", fiber_cl_concentration
        fileHandle.close()
            
    def finish(self):
        # this function may be called at the end of simulation - used very infrequently though
        return
  
class FiberConcentrationCaseBRandom75(SteppableBasePy):
    def __init__(self,frequency=1):
        SteppableBasePy.__init__(self,frequency)
        
    def start(self):
        field_fiber      = self.getConcentrationField("fiber")
        #field_fiber_cl = self.getConcentrationField('fiber_cl')
        
        for x,y,z in self.everyPixel():   
            field_fiber[x,y,z]  = random.uniform(0.5,1)
            #field_fiber_cl[x,y,z]  = 0
                
    def step(self,mcs):
        fiber_concentration     = 1
        #fiber_cl_concentration = 0  
        
        field_fiber      = self.getConcentrationField("fiber")
        #field_fiber_cl = self.getConcentrationField('fiber_cl')
        
        for x,y,z in self.everyPixel():
            fiber_concentration     += field_fiber[x,y,z]
            #fiber_cl_concentration += field_fiber_cl[x,y,z]
                    
        fileName='CaseB_Random_75.csv'
        try:
            fileHandle,fullFileName=self.openFileInSimulationOutputDirectory(fileName,"a")
        except IOError:
            print "Could not open file ", fileName," for writing. "
            return
            
        print >>fileHandle,mcs,",", fiber_concentration #,",", fiber_cl_concentration
        fileHandle.close()
            
    def finish(self):
        # this function may be called at the end of simulation - used very infrequently though
        return
  

class FiberConcentrationCaseCUniform25(SteppableBasePy):
    def __init__(self,frequency=1):
        SteppableBasePy.__init__(self,frequency)
        
    def start(self):
        field_fiber      = self.getConcentrationField("fiber")
        #field_fiber_cl = self.getConcentrationField('fiber_cl')
        
        #****Average 0.25 everywhere:
        for x,y,z in self.everyPixel():  
            field_fiber[x,y,z]  = 1
           # field_fiber_cl[x,y,z]  = 0
        
        #****Half  0 Half 1
        #for x,y,z in self.everyPixel():  
         #   if x >=150: 
         #       field_fiber[x,y,z]  = 0.5
         #       field_fiber_cl[x,y,z]  = 0
         #   else:
         #       field_fiber[x,y,z]  = 0
         #       field_fiber_cl[x,y,z]  = 0
                
    def step(self,mcs):
        fiber_concentration     = 1
        #fiber_cl_concentration = 0  
        
        field_fiber      = self.getConcentrationField("fiber")
        #field_fiber_cl = self.getConcentrationField('fiber_cl')
        
        for x,y,z in self.everyPixel():
            fiber_concentration     += field_fiber[x,y,z]
            #fiber_cl_concentration += field_fiber_cl[x,y,z]
                
      
                    
        fileName='CaseC_Uniform_25.csv'
        try:
            fileHandle,fullFileName=self.openFileInSimulationOutputDirectory(fileName,"a")
        except IOError:
            print "Could not open file ", fileName," for writing. "
            return
            
        print >>fileHandle,mcs,",", fiber_concentration #,",", fiber_cl_concentration
        fileHandle.close()
            
    def finish(self):
        # this function may be called at the end of simulation - used very infrequently though
        return


class FiberConcentrationCaseCUniform75(SteppableBasePy):
    def __init__(self,simulator,frequency=1):
        SteppableBasePy.__init__(self,frequency)
       
    def start(self):
        field_fiber      = self.getConcentrationField("fiber")
        #field_fiber_cl = self.getConcentrationField('fiber_cl')
        
        #****Average 0.25 everywhere:
        for x,y,z in self.everyPixel():  
            field_fiber[x,y,z]  = 0.75
            #field_fiber_cl[x,y,z]  = 0
        
        #****Half  0 Half 1
        for x,y,z in self.everyPixel():  
            if x >=150: 
               field_fiber[x,y,z]  = 1
          #     field_fiber_cl[x,y,z]  = 0
            else:
                field_fiber[x,y,z]  = 0.5
         #       field_fiber_cl[x,y,z]  = 0
                
    def step(self,mcs):
        fiber_concentration     = 0
        #fiber_cl_concentration = 0  
        
        field_fiber      = self.getConcentrationField("fiber")
        #field_fiber_cl = self.getConcentrationField('fiber_cl')
        
        for x,y,z in self.everyPixel():
            fiber_concentration     += field_fiber[x,y,z]
           # fiber_cl_concentration += field_fiber_cl[x,y,z]
                                   
        fileName='CaseC_Uniform_75.csv'
        try:
            fileHandle,fullFileName=self.openFileInSimulationOutputDirectory(fileName,"a")
        except IOError:
            print "Could not open file ", fileName," for writing. "
            return
            
        print >>fileHandle,mcs,",", fiber_concentration#,",", fiber_cl_concentration
        fileHandle.close()
            
    def finish(self):
        # this function may be called at the end of simulation - used very infrequently though
        return
    

class LogData(SteppableBasePy):
    def __init__(self,frequency=10):
        SteppableBasePy.__init__(self,frequency)
        
    def start(self):   
        IDCount = 1

        for cell in self.cellListByType(1):
            cell_attribute=self.getDictionaryAttribute(cell)
                
            # Way to count the amount of generalized cells for a given cell type 
            cell_attribute["id"] = IDCount 
            IDCount += 1
       
    def step(self,mcs):
        
        #*****For every cell_ID of the same cell type, log cell position in term of xCOM and yCOM
        for cell in self.cellListByType(1):
            cell_attribute=self.getDictionaryAttribute(cell)

            #Log data into multiple separate cvs file by cell_ID                      
            #fileName='CellPosition_COM_'+str(cell_attribute["id"])+'.csv'

            #Log all data into one cvs file
            fileName='CellPosition_COM.csv'
            
            try:
                fileHandle,fullFileName=self.openFileInSimulationOutputDirectory(fileName,"a")
            except IOError:
                print "Could not open file ", fileName," for writing. "
                return
            
            cell_attribute=self.getDictionaryAttribute(cell)
            print >>fileHandle,cell.id,",",mcs,",",cell.xCOM,",",cell.yCOM
            fileHandle.close()
            
        #******Log MMP and LOX Concentration 
        #MMP = 0
        #fieldMMP=self.getConcentrationField("MMP")
        #for x in xrange(self.dim.x):             
         #   for y in xrange(self.dim.y):                 
         #       for z in xrange(self.dim.z): 
         #           MMP += fieldMMP[x,y,z];
                    
        #fileName='MMP_LOX.csv'
        #try:
        #    fileHandle,fullFileName=self.openFileInSimulationOutputDirectory(fileName,"a")
        #except IOError:
          #  print "Could not open file ", fileName," for writing. "
         #   return
            
        #print >>fileHandle,mcs,",", MMP
        #fileHandle.close()    
            
    def finish(self):
        # this function may be called at the end of simulation - used very infrequently though
        return
    
class ChemotaxisTest(SteppableBasePy):
    def __init__(self,frequency=1):
        SteppableBasePy.__init__(self,frequency)
        
    def start(self):
        field_fiber      = self.getConcentrationField("fiber")
        #field_fiber_cl = self.getConcentrationField('fiber_cl')
        
         #for x,y,z in self.everyPixel():  
         #   field_fiber[x,y,z]  = 0.5
         #   field_fiber_cl[x,y,z]  = 0

        # The half uncrosslink and half crosslink test
        for x,y,z in self.everyPixel():  
            if x >=150:
                #field_fiber[x,y,z]  = random.uniform(0,1)
                field_fiber[x,y,z]  = 0.5
                #field_fiber_cl[x,y,z]  = 0
            else:
                field_fiber[x,y,z]  = 1 # Changed this from zero to one
                #field_fiber_cl[x,y,z]  = random.uniform(0,1)
                #field_fiber_cl[x,y,z]  = 0.5
                
    def step(self,mcs):
        pass
                           
    def finish(self):
        # this function may be called at the end of simulation - used very infrequently though
        return
    
class FiberConcentrationCaseCircular(SteppableBasePy):
   def __init__(self,frequency=1):
       SteppableBasePy.__init__(self,frequency)
       
       
   def start(self):
       field_fiber = self.getConcentrationField("fiber")
       #**** 1 outside of radius = blob radius
       for x,y,z in self.everyPixel(): 
           if math.sqrt((x-150)**2+(y-150)**2) <= 64: # edit this if origin is not at 0,0
               field_fiber[x,y,z]  = 1.0
               #field_fiber_cl[x,y,z]  = 0
           else:
               field_fiber[x,y,z]  = 0
               #field_fiber_cl[x,y,z]  = 0
               
       #for cell in self.cellList:
           #if cell.type == self.everyPixel:
               
               #field_fiber = 0
          #field_fiber_cl[x,y,z]  = 0
           #else:
               
               #field_fiber  = 1
          #field_fiber_cl[x,y,z]  = 0
              
   def step(self,mcs):
       fiber_concentration     = 0
       #fiber_cl_concentration = 0 
      
       field_fiber      = self.getConcentrationField("fiber")
       #field_fiber_cl = self.getConcentrationField('fiber_cl')
      
       for x,y,z in self.everyPixel():
           fiber_concentration     += field_fiber[x,y,z]
           #fiber_cl_concentration += field_fiber_cl[x,y,z]
              
                     
       fileName='CaseCircular_Uniform.csv'
       try:
           fileHandle,fullFileName=self.openFileInSimulationOutputDirectory(fileName,"a")
       except IOError:
           print "Could not open file ", fileName," for writing. "
           return
          
       print >>fileHandle,mcs,",", fiber_concentration #,",", fiber_cl_concentration
       fileHandle.close()
          
   def finish(self):
       # this function may be called at the end of simulation - used very infrequently though
       return
       
       
     

class ExtraPlotSteppable(SteppableBasePy):
    def __init__(self,frequency=10):
        SteppableBasePy.__init__(self,frequency)

    def start(self):

        self.pW=self.addNewPlotWindow(_title='Average Volume And Surface',_xAxisTitle='MonteCarlo Step (MCS)',_yAxisTitle='Variables', _xScaleType='linear',_yScaleType='linear',_grid=False)
		
        self.pW.addPlot("MVol", _style='Lines',_color='red')
        self.pW.addPlot("MSur", _style='Dots',_color='green')
        
        # adding automatically generated legend
        # default possition is at the bottom of the plot but here we put it at the top
        self.pW.addAutoLegend("top")

        self.clearFlag = False

    def step(self, mcs):
        if not self.pW:
            print "To get scientific plots working you need extra packages installed:"
            print "Windows/OSX Users: Make sure you have numpy installed. For instructions please visit www.compucell3d.org/Downloads"
            print "Linux Users: Make sure you have numpy and PyQwt installed. Please consult your linux distributioun manual pages on how to best install those packages"
            return
            # self.pW.addDataPoint("MCS",mcs,mcs)

        # self.pW.addDataPoint("MCS1",mcs,-2*mcs)
        # this is totall non optimized code. It is for illustrative purposes only. 
        meanSurface = 0.0
        meanVolume = 0.0
        numberOfCells = 0
        for cell in self.cellList:
            meanVolume += cell.volume
            meanSurface += cell.surface
            numberOfCells += 1
        meanVolume /= float(numberOfCells)
        meanSurface /= float(numberOfCells)



        if mcs > 100 and mcs < 200:
            self.pW.eraseAllData()
        else:
            self.pW.addDataPoint("MVol", mcs, meanVolume)
            self.pW.addDataPoint("MSur", mcs, meanSurface)
            if mcs >= 200:
                print "Adding meanVolume=", meanVolume
                print "plotData=", self.pW.plotData["MVol"]


        self.pW.showAllPlots()

        # Saving plots as PNG's
        if mcs < 50:
            qwtPlotWidget = self.pW.getQWTPLotWidget()
            qwtPlotWidgetSize = qwtPlotWidget.size()
            # print "pW.size=",self.pW.size()
            fileName = "ExtraPlots_" + str(mcs) + ".png"
            self.pW.savePlotAsPNG(fileName, 550, 550)  # here we specify size of the image saved - default is 400 x 400


class ExtraMultiPlotSteppable(SteppableBasePy):
    def __init__(self,frequency=10):
        SteppableBasePy.__init__(self,frequency)

    def start(self):

        self.pWVol=self.addNewPlotWindow(_title='Average Volume ',_xAxisTitle='MonteCarlo Step (MCS)',_yAxisTitle='Volume', _xScaleType='linear',_yScaleType='linear')
        
        self.pWVol.addPlot("MVol", _style='Dots',_color='blue')
        
        if not self.pWVol:
            return
        
        # adding automatically generated legend
        self.pWVol.addAutoLegend()
       
        self.pWSur=self.addNewPlotWindow(_title='Average Surface',_xAxisTitle='MonteCarlo Step (MCS)',_yAxisTitle='Surface', _xScaleType='linear',_yScaleType='linear')
        self.pWSur.addPlot("MSur", _style='Dots', _color='red')


    def step(self, mcs):
        # this is totally non optimized code. It is for illustrative purposes only. 
        meanSurface = 0.0
        meanVolume = 0.0
        numberOfCells = 0
        for cell in self.cellList:
            meanVolume += cell.volume
            meanSurface += cell.surface
            numberOfCells += 1
        meanVolume /= float(numberOfCells)
        meanSurface /= float(numberOfCells)

        self.pWVol.addDataPoint("MVol", mcs, meanVolume)
        self.pWSur.addDataPoint("MSur", mcs, meanSurface)
        print "meanVolume=", meanVolume, "meanSurface=", meanSurface

        # self.pW.showPlot("MCS1")
        self.pWVol.showAllPlots()
        self.pWSur.showAllPlots()
